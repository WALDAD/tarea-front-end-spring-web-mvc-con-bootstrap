package com_materias.api_calificaciones.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class calificaciones {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
    private String materia;
    private String docente;
    private double nota;
    private Integer periodo;

    public calificaciones(){
        
    }

    public calificaciones(long id, String materia, String docente, double nota, Integer periodo) {
        this.id = id;
        this.materia = materia;
        this.docente = docente;
        this.nota = nota;
        this.periodo = periodo;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getMateria() {
        return materia;
    }

    public void setMateria(String materia) {
        this.materia = materia;
    }

    public String getDocente() {
        return docente;
    }

    public void setDocente(String docente) {
        this.docente = docente;
    }

    public double getNota() {
        return nota;
    }

    public void setNota(double nota) {
        this.nota = nota;
    }

    public Integer getPeriodo() {
        return periodo;
    }

    public void setPeriodo(Integer periodo) {
        this.periodo = periodo;
    }

}
