package com_materias.api_calificaciones;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DemoCalificaciones {

	public static void main(String[] args) {
		SpringApplication.run(DemoCalificaciones.class, args);
	}

}
