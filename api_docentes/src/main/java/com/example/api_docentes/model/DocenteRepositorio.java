package com.example.api_docentes.model;



import org.springframework.data.repository.CrudRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

//Remove @RepositoryRestResource below to disable auto REST api:
@RepositoryRestResource
public interface DocenteRepositorio extends CrudRepository<Docente, Long>{}


