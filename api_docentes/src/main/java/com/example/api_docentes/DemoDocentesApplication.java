package com.example.api_docentes;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DemoDocentesApplication {

	public static void main(String[] args) {
		SpringApplication.run(DemoDocentesApplication.class, args);
	}

}
