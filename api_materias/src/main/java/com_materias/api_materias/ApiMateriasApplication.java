package com_materias.api_materias;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ApiMateriasApplication {

	public static void main(String[] args) {
		SpringApplication.run(ApiMateriasApplication.class, args);
	}

}
